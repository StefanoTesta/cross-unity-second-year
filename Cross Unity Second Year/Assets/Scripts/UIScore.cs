using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIScore : MonoBehaviour
{
    public TextMeshProUGUI Score;
    public TextMeshProUGUI BestScore;


    void Update()
    {
        Score.text = "Score: " + PlayerPrefs.GetInt("Score").ToString();
        BestScore.text = "BestScore: " + PlayerPrefs.GetInt("BestScore").ToString();
    }
}
