using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    public float speed;
    public GameObject HighestBoundary;
    public GameObject LowestBoundary;

    public GameObject bulletPrefab;

   
    void Update()
    {

        //Movment Up
        if(Input.GetKey(KeyCode.W))
        {
                Move(Vector3.up);
        }

        //Movement Down
        else if (Input.GetKey(KeyCode.S))
        {
               Move(Vector3.down);
        }
      
        if(Input.GetKeyDown(KeyCode.Space))
            {
                Shoot();
            }



       void Shoot()
        {
            Instantiate (bulletPrefab).transform.position = transform.position;
        }

        void Move (Vector3 direction)
        {
            transform.position += direction * speed * Time.deltaTime;


            if (direction.Equals(Vector3.up) && transform.position.y > HighestBoundary.transform.position.y)
            {
                transform.position = new Vector3(transform.position.x, HighestBoundary.transform.position.y, transform.position.z);
            }

            else if (direction.Equals(Vector3.down) && transform.position.y < LowestBoundary.transform.position.y)
            {
                transform.position = new Vector3 (transform.position.x, LowestBoundary.transform.position.y, transform.position.z);
            }
        }
    }

   
}



