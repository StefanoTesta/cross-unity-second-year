using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bulletSpeed;

    void Start()
    {
        Destroy( this.gameObject, 10f);  
    }

    void Update()
    {
        transform.position += Vector3.right * Time.deltaTime * bulletSpeed;
    }
}
