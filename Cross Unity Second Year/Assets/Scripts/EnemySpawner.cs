using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject Enemy;
    public GameObject MaxEnemySpawner;
    public GameObject MinEnemySpawner;
    private float SpawnTimer;
    public float MaxSpawnTimer;
    public float MinSpawnTimer;

    void Start()
    {
        SpawnTimer = 2;
    }

    
    void Update()
    {
        if (SpawnTimer>0)
        {
            SpawnTimer -= Time.deltaTime;

            if (SpawnTimer<=0)
            {
                spawnEnemy();
                SpawnTimer = Random.Range(MinSpawnTimer, MaxSpawnTimer);
            }
        }
    }

    void spawnEnemy()
    {
        Instantiate(Enemy, new Vector3(transform.position.x, Random.Range(MinEnemySpawner.transform.position.y, MaxEnemySpawner.transform.position.y), transform.position.z),transform.rotation);
    }
}
