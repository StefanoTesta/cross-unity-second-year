using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public float enemySpeed;
    public float secondToDestroy;
  
    void Start()
    {
        Destroy(this.gameObject, secondToDestroy);
    }

  
    void Update()
    {
        transform.Translate(Vector3.left * enemySpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D (Collider2D collision)
    {
        if (collision.tag == "Bullet")
        {
            Destroy(this.gameObject);
            Destroy(collision.gameObject);

            int currentScore = PlayerPrefs.GetInt("Score");

            PlayerPrefs.SetInt("Score",currentScore + 1);

            print(PlayerPrefs.GetInt("Score"));
        }
        
        if(collision.tag == "Player"|| collision.tag == "GameOver")
        {
            if (PlayerPrefs.GetInt("Score") > PlayerPrefs.GetInt("BestScore"))
            {
                PlayerPrefs.SetInt("BestScore", PlayerPrefs.GetInt("Score"));
            }

            PlayerPrefs.SetInt("Score", 0);

            UnityEngine.SceneManagement.SceneManager.LoadScene("GameScene");

            print(PlayerPrefs.GetInt("BestScore"));
        }
    }
}
